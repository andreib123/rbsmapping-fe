import { Component, OnInit, ViewChild } from '@angular/core';
import { SearchsidebarService } from '../searchsidebar.service'
import { Observable, Subject } from 'rxjs'
import data from './cities.json';
import states from './states.json';




@Component({
  selector: 'app-automove',
  templateUrl: './automove.component.html',
  styleUrls: ['./automove.component.css']
})
export class AutomoveComponent implements OnInit {


  
  constructor(private searchsb:SearchsidebarService) {};
  
  //public stateinput = {};

  public stateinput;
  public cityinput;

  receiveStateInput(){
    this.searchsb.getStateInput$.subscribe((stateinput) => {this.stateinput = stateinput;});
  }

  receiveCityInput(){
    this.searchsb.getCityInput$.subscribe((cityinput) => {this.cityinput = cityinput;});
  }

  //deprecated
  PopulateStateDropDown()
  {
    console.log(states[5]);
    var dropDown = document.getElementById("statelist");
    var rbsstatearr = ["Washington", "Oregon", "California", "Nevada", "North Dakota", "South Dakota", "Nebraska", "Kansas", "Missouri", "Iowa", "Minnesota",
                        "Wisconsin", "Illinois", "Michigan", "Indiana", "Kentucky", "Tennessee", "Alabama", "Florida", "Georgia", "South Carolina", "North Carolina",
                        "Alaska", "Hawaii"];
    var statearr = [];
    for(let i = 0; i < data.length; i++)
    {
      if((statearr.includes(data[i].state) == false) && (rbsstatearr.includes(data[i].state)))
      {
        statearr.push(data[i].state);
      }
    }
    statearr.sort();
    for(let j = 0; j < statearr.length; j++)
    {
      var arrayOpt = statearr[j];
      var dropDownOpt = document.createElement("option");
      dropDownOpt.textContent = arrayOpt;
      dropDownOpt.value = arrayOpt;
      dropDown.appendChild(dropDownOpt);
    }
  }

    
  
  //deprecated
  StateSelectionHandler()
  {
    var dropDown = document.getElementById("statelist") as HTMLSelectElement;
    //var selection = this.stateInput;
    let selection = "California";
    return selection;
  }

  //deprecated
  PopulateCityDropDown()
  {
    
    //console.log(this.stateInput);
    var stateselection = this.StateSelectionHandler();
    var dropDown = document.getElementById("citylist") as HTMLSelectElement;
    var cityarray = [];
    cityarray[0] = "Zoom to State";
    for(let i = 1; i < data.length; i++)
    {
        if(data[i].state == stateselection)
          {
            cityarray.push(data[i].city);
          }
      
    }
    cityarray.sort();
    if(dropDown.length == 1)
    {
      for(let i = 0; i < cityarray.length; i++)
      {
    
            var arrayOpt = cityarray[i];
            var dropDownOpt = document.createElement("option");
            dropDownOpt.textContent = arrayOpt;
            dropDownOpt.value = arrayOpt;
            dropDown.appendChild(dropDownOpt);
  
      }
    }
    else
    {
      for(let i = 0; i < dropDown.length; i ++)
      {
        dropDown.textContent = null;
        dropDown.value = null;
      }
      for(let i = 0; i < cityarray.length; i++)
      {
      
            var arrayOpt = cityarray[i];
            var dropDownOpt = document.createElement("option");
            dropDownOpt.textContent = arrayOpt;
            dropDownOpt.value = arrayOpt;
            dropDown.appendChild(dropDownOpt);
        
      }
    }
    




  }

  //deprecated
  SelectionHandler()
  {
    var dropDown = document.getElementById("citylist") as HTMLSelectElement;
    var selection = dropDown.options[dropDown.selectedIndex].value;
    return selection;
  }

  GetAreaType()
  {
    var loc = this.cityinput;
    if ((loc == undefined) || (loc == ""))
    {
      var areatype = "State";
    }
    else 
    {
      var areatype = "City"
    }
    console.log(areatype);
    return areatype;
  }

  GetLat()
  {
    let cityname = this.cityinput;
    console.log(cityname);
    let statename = this.stateinput;
    if((cityname == "") || (cityname == undefined))
    {
      for(let i = 0; i < states.length; i++)
      {
        if (states[i].state == statename)
        {
          console.log(statename);
          let ilat:number = states[i].latitude;
          return ilat;
        }
      }
    }
    else
    {
      for(let i = 0; i < data.length; i++)
      {
        if ((data[i].city == cityname) && (data[i].state == statename))
        {
          let ilat:number = data[i].latitude;
          return ilat;
        }
      }
    }
  }

  GetLng()
  {
    let cityname = this.cityinput;
    let statename = this.stateinput;
    if((cityname == "") || (cityname == undefined))
    {
      for(let i = 0; i < states.length; i++)
      {
        if (states[i].state == statename)
        {
          let ilng:number = states[i].longitude;
          return ilng;
        }
      }
    }
    else
    {
      for(let i = 0; i < data.length; i++)
      {
        if ((data[i].city == cityname) && (data[i].state == statename))
        {

          let ilng = data[i].longitude;
          return ilng;
        }
      }
    }
  }


  ngOnInit()
  {

    //this.PopulateStateDropDown();
    //let sln1 = document.getElementById("statelist");
    //sln1.addEventListener('change', (e:Event) => this.PopulateCityDropDown());
    
    //let btn = document.getElementById("go");
    //btn.addEventListener('click', (e:Event) => {
    //   console.log("Received:"+this.stateinput);
    //   console.log("Received:"+this.cityinput);
    //   console.log(this.GetLat())
    //   console.log(this.GetLng())
    //   console.log(this.GetAreaType())
    //  }
    //)
    
    //sln1.addEventListener('change', (e:Event) => this.PopulateCityDropDown());


  
  }

}
