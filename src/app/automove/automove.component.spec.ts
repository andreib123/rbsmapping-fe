import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomoveComponent } from './automove.component';

describe('AutomoveComponent', () => {
  let component: AutomoveComponent;
  let fixture: ComponentFixture<AutomoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomoveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
