import { HomeComponent } from './../home/home.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as ol from 'openlayers'
import { RbsMap } from '../model/rbs-map.model';
import { ToastrNotificationService } from '../toastr-notification.service';
import { MapFunctionalityService } from '../map-functionality.service';

var searchJson: any[] | { display_name: { includes: (arg0: string) => void; }; }[];

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {
  @Input() RbsMap: RbsMap;
  @Input() map: ol.Map;
  @Input() source: ol.source.Vector;
  @Input() notifyServ: ToastrNotificationService;
  @Output() findFeature = new EventEmitter();
  searching = false;
  lonJSON: string | number;
  latJSON: string | number;
  constructor(private mapFunctions: MapFunctionalityService, private HomeComponent: HomeComponent) { }

  ngOnInit() {
  }

// Puts together th3e request to send to the back-end
  searchStart(event: { keyCode: number; }) {
    if (event.keyCode === 13) {
      // regex used to test if the request is a cgi
      let regexp = new RegExp('^[0-9]{3}[-][0-9]{3}[-][0-9]{0,6}[-][0-9]{0,3}$');
      var searchRequest = "";
      var searchInput = (<HTMLInputElement>document.getElementById("searchInput")).value;
      // if request is cgi, it's parsed to find the enodeBID
      if (regexp.test(searchInput)) {
        var split = searchInput.split('-');
        searchRequest = split[2];
        if (searchRequest.length < 6) {
          var count = 6 - searchRequest.length;
          for (var i = 1; i <= count; i++) {
            searchRequest = '0' + searchRequest;
          }
        }
      }
      // if not cgi, it just goes on ahead with the enodeBID
      else {
        searchRequest = searchInput;
      }
      var found = false;
      // searches each rbs till it finds the one with the enodeBID
      for (var i = 0; i < this.RbsMap.rbss.length; i++) {
        if (this.RbsMap.rbss[i].eNodeBID == searchRequest) {
          this.lonJSON = (Math.round(parseFloat(this.RbsMap.rbss[i].longitude) * 10000) / 10000);
          this.latJSON = (Math.round(parseFloat(this.RbsMap.rbss[i].latitude) * 10000) / 10000);
          var features = this.source.getFeatures();
          // all features are searched to find the one that matches the RBS's location
          for (var j = 0; j < features.length; j++) {
            // feature is checked to see if it is a cluster
            found = this.isCluster(features[j]);
            // once the feature is found the for loop is broken
            if (found == true) {
              break;
            }
          }
        }
      }
    }
  }

  isCluster(feature: ol.Feature): boolean {
    // if feature is not a cluster it runs the search on the feature
    if (!feature || !feature.get('features')) {
      return this.searchFeature(feature.get('features')[f])
    }
      // if feature is a cluster it runs the search on all the features in that cluster
    else if (feature || feature.get('features')) {
      for (var f = 0; f < feature.get('features').length; f++) {
        if (this.searchFeature(feature.get('features')[f])) {
          return true;
        }
      }
      return false;
    }
  }

  searchFeature(featureF: { getGeometry?: any; }): boolean {
    // get lat lon of the feature
    var feature: ol.geom.Point = <ol.geom.Point>featureF.getGeometry();
    var coordf: ol.Coordinate = feature.getCoordinates();
    coordf = ol.proj.transform(coordf, 'EPSG:3857', 'EPSG:4326');
    coordf[0] = Math.round(coordf[0] * 10000) / 10000;
    coordf[1] = Math.round(coordf[1] * 10000) / 10000;
    // check if lat lon of feature matches lat lon of rbs
    if (coordf[0] == this.lonJSON && coordf[1] == this.latJSON) {
      (<HTMLInputElement>document.getElementById("searchInput")).value = "";
      // if they match move map to the rbs's feature
      this.map.getView().animate({
        zoom: 14,
        duration: 1000,
        center: ol.proj.fromLonLat([this.lonJSON, this.latJSON])
      });
      // call to home component to select the feature, opening the side nav with the rbs's information
      this.findFeature.emit(featureF);
      // tell the methods above it's been found and to break
      return true;
    }
    // tell the methods above to keep searching
    return false;
  }


  enterHit(event: { keyCode: number; }) {
    if (event.keyCode === 13) {
      this.moveTo();
    }
  }

  moveTo() {
    try {
      var duration = 1500;
      var zoomVal = 11;
      var zoomout = 4;
      var durationOut = 700;
      var currLoc = ol.proj.transform(this.map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
      var add1 = searchJson[0].display_name;
      var type1 = searchJson[0].type;
      var addressLonLat = [];
      addressLonLat[0] = searchJson[0].lon;
      addressLonLat[1] = searchJson[0].lat;
      var lon: number = Math.round(addressLonLat[0] * 10000) / 10000
      var lat: number = Math.round(addressLonLat[1] * 10000) / 10000;
      var distance = this.mapFunctions.getDistanceFromLatLonInKm([currLoc[0], currLoc[1], lon, lat])
      if (distance <= 50) {
        //zoomout = zoom;
        durationOut = 0;
      }
      if (type1 == "administrative") {
        zoomVal = 6.5;
        duration = 700
      }
      else if (type1 == "city") {
        zoomVal = 12;
        duration = 1750
      }
      else if (type1 == "hamlet" || type1 == "village") {
        zoomVal = 14;
        duration = 2000
      }
      else if (type1 == "house" || type1 == "residential" || type1 == "tertiary" || type1 == "service" || type1 == "commerical" || type1 == "building" || type1 == "attraction") {
        zoomVal = 15;
        duration = 3000;
      }
      var duration1 = 1500;
      if (distance > 500) {
        duration1 = log(distance) * 100;
      }
      this.map.getView().animate({
        zoom: zoomout,
        duration: durationOut,
      },
        {
          center: ol.proj.fromLonLat([lon, lat]),
          duration: duration1,
        }, {
          zoom: zoomVal,
          duration: duration,
        });
      var input = <HTMLInputElement>document.getElementById("AddressSearch")
      input.value = ""
      var op1 = document.getElementById("opt1")
      var op2 = document.getElementById("opt2")
      var op3 = document.getElementById("opt3")
      op1.innerText = ""
      op2.innerText = ""
      op3.innerText = ""
      this.notifyServ.showInfo("Address: " + add1)
      this.HomeComponent.addFeat(lon, lat);
      searchJson = [];
    }
    catch{
      this.notifyServ.showError("Invalid Address")
      var input = <HTMLInputElement>document.getElementById("AddressSearch")
      input.value = ""
    }
  }

  getAddress() {
    var input = (<HTMLInputElement>document.getElementById("AddressSearch")).value
    var usa = ",USA";
    var _this = this
    if (input.includes("usa") || input.includes("United States of America") || input.includes("USA")) {
      usa = ""
    }
    if (input.length > 3) {
      if (regex("-[0-9][0-9]", input) == true) {
        if (input.includes(",")) {
          var lon = parseFloat(input.substr(0, input.indexOf(',')).trim())
          var lat = parseFloat(input.substr(input.indexOf(",") + 1).trim())
        } if (typeof lon !== 'undefined' && typeof lat !== 'undefined') {
          if (lon.toString().length > 7 && lat.toString().length > 6) {
            this.map.getView().animate({
              zoom: 3,
              duration: 700
            }, {
                center: ol.proj.fromLonLat([lon, lat]),
                duration: 2500,
              }, {
                zoom: 14,
                duration: 2000
              }
            )
            var url = 'http://nominatim.openstreetmap.org/reverse?format=json&lon=' + lon + '&lat=' + lat
            fetch(url).then(function(response) {
              return response.json();
            }).then(function(json) {
              _this.notifyServ.showInfo("Address: " + json.display_name);
            })
          }
        }
      }
      else {
        var url = "https://nominatim.openstreetmap.org/?format=json&q=" + input + usa + "&limit=3&email=kalem.byrne@ericsson.com";
        fetch(url).then(function(response) {
          return response.json();
        }).then(function(json) {
          searchJson = json;
          try {
            var op1 = document.getElementById("opt1")
            var op2 = document.getElementById("opt2")
            var op3 = document.getElementById("opt3")
            op1.innerText = searchJson[0].display_name
            if (searchJson.length >= 2) {
              op2.innerText = searchJson[1].display_name
            }
            if (searchJson.length >= 3) {
              op3.innerText = searchJson[2].display_name
            }
            if (searchJson[0].display_name == input || searchJson[1].display_name == input || searchJson[2].display_name == input) {
              op1.innerText = ""
              op2.innerText = ""
              op3.innerText = ""
              _this.moveTo();
            }
          }
          catch{
            try {
              if (regex("[0-9], USA", input) == true || input.includes("Islands territorial waters") || regex("[0-9], United States of America", input) == true) {
                var remove = 0;
                if (input.includes("Islands territorial waters")) { remove = 46 }
                else if (input.includes("United States of America")) { remove = 33 }
                else if (input.includes("USA")) { remove = 12 }
                input = input.substring(0, input.length - remove)
                var url = "https://nominatim.openstreetmap.org/?format=json&q=" + input + "&limit=3&email=kalem.byrne@ericsson.com";
                fetch(url).then(function(response) {
                  return response.json();
                }).then(function(json) {
                  searchJson = json
                  if (searchJson[0].display_name.includes(input) || searchJson[1].display_name.includes(input) || searchJson[2].display_name.includes(input)) { _this.moveTo() }
                })
              }
            }
            catch{
              console.log("get address error");
            }
          }
        })
      }
    }
  }
}

function regex(expression: any, input: string) {
  var regexp = new RegExp(expression, "i");
  var boolean = regexp.test(input);
  return boolean;
}

function log(val: number) {
  return Math.log(val) / Math.log(1.5);
}
