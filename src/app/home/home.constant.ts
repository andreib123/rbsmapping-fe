export class Constants {
  static XML_MAP = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
    "<map>" +
    "  <rbs>" +
    "    <area>Summerhill</area>" +
    "    <siteid>01</siteid>" +
    "    <location>" +
    "      <latitude>53.404861</latitude>" +
    "      <longitude>-7.998116</longitude>" +
    "    </location>" +
    "    <cell>" +
    "      <operator>Vodafone</operator>" +
    "      <operatorcellid>01</operatorcellid>" +
    "      <azimuth>280</azimuth>" +
    "      <services>" +
    "        <value>GSM</value>" +
    "        <value>UMTS</value>" +
    "      </services>" +
    "    </cell>" +
    "    <cell>" +
    "      <operator>Vodafone</operator>" +
    "      <operatorcellid>02</operatorcellid>" +
    "      <azimuth>90</azimuth>" +
    "      <services>" +
    "        <value>GSM</value>" +
    "        <value>UMTS</value>" +
    "      </services>" +
    "    </cell>" +
    "  </rbs>" +
    "</map>"
}
