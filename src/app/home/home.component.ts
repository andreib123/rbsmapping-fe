import { Component, OnInit, AfterViewInit, ViewChild, HostBinding } from '@angular/core'
import * as ol from 'openlayers'
import { Constants } from './home.constant'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataService } from '../data.service';
import { MapFunctionalityService } from '../map-functionality.service'
import { SearchsidebarService } from '../searchsidebar.service'
import { MatSidenav, MatDialog, MatDrawer, MatCheckbox } from '@angular/material';
import { AutomoveComponent } from '../automove/automove.component'
import { Observable, Subject } from 'rxjs'
import { RbsMap } from '../model/rbs-map.model';
import { RbsForInitView } from '../model/rbs-for-init-view.model';
import { Cell } from '../model/cell.model';
import { TableService } from '../table.service';
import { ToastrNotificationService } from '../toastr-notification.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FeatureSetupService } from '../feature-setup.service';

var clickedFeat;
var newFeat;
var measureFeat = [];
var zoom;
var zoomSource
var zoomLayer
var aerialTile;
var openTile;
var satelliteTile;
var roadTile;
var vectorLayer
var clusterSource

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  filteredVectorLayer: ol.layer.Vector;
  rbs: RbsForInitView;
  cgiList: string[];
  cellList: Cell[];
  setCell: Cell;
  isOpen = false;
  colourBlind: boolean = this.mapFunctions.getColourBlind();
  map: ol.Map
  xmlDocument: Document
  rbsMap: RbsMap
  vectorSource: ol.source.Vector;
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('endnav') endnav: MatSidenav;
  @ViewChild('searchsidebar') searchsidebar: MatDrawer;
  @ViewChild('colourBlindBtn') cBBtn: MatCheckbox;
  tableGeohash: string;

  constructor(private data: DataService, public dialog: MatDialog,
    private searchsidebarServe: SearchsidebarService, private table: TableService,
    private notifyServ: ToastrNotificationService, private mapFunctions: MapFunctionalityService, private spinner: NgxSpinnerService,
    private featureSet: FeatureSetupService) { }
  mapMover = new AutomoveComponent(this.searchsidebarServe);
  notice: ToastrNotificationService = this.notifyServ;
  featureSetup = this.featureSet;

  ngOnInit() {
    if (this.colourBlind == true) {
      this.cBBtn.checked = true;
    }
    var homeComponent = this;
    // start the spinner while the map loads
    this.spinner.show();
    this.searchsidebarServe.setSearchsidebar(this.searchsidebar);
    this.createMap()
    roadTile = this.map.getLayers().getArray()[0]
    satelliteTile = this.map.getLayers().getArray()[1]
    if (this.mapFunctions.getRoad() === true) {
      satelliteTile.setVisible(false)
      roadTile.setVisible(true);
    } else if (this.mapFunctions.getRoad() === false) {
      satelliteTile.setVisible(true)
      roadTile.setVisible(false);
    }
    // sets the rbsMap that contains all the rbss
    this.rbsMap = this.mapFunctions.rbsMap
    if (this.rbsMap == null) {
      this.data.getFullRbsMapInfo().then((rbsMap: RbsMap) => {
        this.rbsMap = rbsMap
        // adds the layers to the map that contain the features
        this.addLayers(this.getFeaturesArray())
        // adds event listeners to features & buttons that can't be done with angular
        homeComponent.addMapEventListeners()
      })
    }
    else {
      this.addLayers(this.getFeaturesArray())
      this.addMapEventListeners()
    }
    this.receiveMeasureButtonStatusInput();
    this.receiveAddressButtonStatusInput();
  }

  // sets the view to road or satellite based on the users choice
  setView(e: Event) {
    if (e.srcElement.innerHTML == "Road") {
      aerialTile.setVisible(false)
      openTile.setVisible(true);
    }
    else {
      aerialTile.setVisible(true)
      openTile.setVisible(false);
    }
  }

  // zooms the map in if the user presses the zoom button
  zoom(e: Event) {
    if (e.srcElement.innerHTML.includes("zoom_out")) {
      this.map.getView().animate({ zoom: zoom - 1, duration: 300 });
    }
    else {
      this.map.getView().animate({ zoom: zoom + 1, duration: 300 });
    }
  }

  // closes the sidenav and the endnav if the user clicks the close button
  closeNav() {
    this.endnav.close();
    this.sidenav.close();
    zoomLayer.getSource().clear();
    zoomLayer.setVisible(false);
    document.getElementById("map").style.opacity = "1.0";
  }

  // send the cell info from the sidenav select to the endnav
  setCellInfo(cell: Cell) {
    this.endnav.close();
    this.setCell = cell;
    console.log(this.setCell);
    setTimeout(() => {
      this.endnav.open();
    }, 500);
  }

  getFeaturesArray(): any[] {
    var featuresArray = []
    for (var i = 0; i < this.rbsMap.rbss.length; i++) {
      featuresArray.push(
        new ol.Feature({
          geometry: new ol.geom.Point(
            ol.proj.fromLonLat(
              [parseFloat(this.rbsMap.rbss[i].longitude),
              parseFloat(this.rbsMap.rbss[i].latitude)]
            )
          ),
          name: parseFloat(this.rbsMap.rbss[i].numCells)
        })
      )
    }
    return featuresArray
  }

  createMap() {
    //creation of map with layers
    this.map = new ol.Map({
      loadTilesWhileInteracting: true,
      target: 'map',
      layers: [
        new ol.layer.Tile({
          preload: Infinity,
          source: new ol.source.BingMaps({
            key: 'Arl_aEWtGGTg5jyRe_EOsmDfS8Ac4G_l0xajmNT6xcu86m66m8-dmzEvJBPv0ozr ',
            imagerySet: 'Road',
          }),
          zIndex: -2000,
          visible: true
        }),
        new ol.layer.Tile({
          preload: Infinity,
          source: new ol.source.BingMaps({
            key: 'Arl_aEWtGGTg5jyRe_EOsmDfS8Ac4G_l0xajmNT6xcu86m66m8-dmzEvJBPv0ozr ',
            imagerySet: 'AerialWithLabels',
          }),
          zIndex: -1000,
          visible: false
        })
        // vectorLayer
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([-105.049352, 42.958874]),
        zoom: 4.4,
        maxZoom: 20.5,
        minZoom: 3.5
      })
    })
  }

  // sets the cluster colours to a more colour blind friendly colour if clicked
  colourBlindMode() {
    var zoom: number = this.map.getView().getZoom();
    this.colourBlind = !this.colourBlind;
    console.log(this.colourBlind);
    if (this.colourBlind == true) {
      zoom = zoom + .1;
    }
    else if (this.colourBlind == false) {
      zoom = zoom - .1;
    }
    this.map.getView().animate({
      zoom: zoom,
      duration: 400
    });
  }

  // add the layers to the map
  addLayers(rbsFeaturesArray: any[]) {
    var homeComponent = this;
    var layers = [new ol.layer.Vector({
      // clusterSource
      source: new ol.source.Cluster({
        distance: 50, // cluster size
        // vectorSource
        source: new ol.source.Vector({
          features: rbsFeaturesArray
        })
      }),
      style: function(feature) {
        var style = homeComponent.featureSet.setStyle(feature, homeComponent.colourBlind)
        return style;
      }
    }),
    // zoomLayer
    new ol.layer.Vector({
      // zoomSource
      source: new ol.source.Vector({}),
      zIndex: 2000,
    }, )]
    for (var i = 0; i < layers.length; i++) {
      this.map.addLayer(layers[i]);
    }
    this.map.getView().setZoom(4.5)
    openTile = this.map.getLayers().getArray()[0]
    aerialTile = this.map.getLayers().getArray()[1]
    vectorLayer = this.map.getLayers().getArray()[2]
    zoomLayer = this.map.getLayers().getArray()[3]
    clusterSource = vectorLayer.getSource();
    zoomSource = zoomLayer.getSource();
    this.notifyServ.showSuccess("Map loaded successfully")
    this.vectorSource = vectorLayer.getSource();
    this.spinner.hide();
    this.map.getLayers().getArray()[2].set('name', "vectorLayer");
  }

  public measurebuttonstatusinput;
  public addressbuttonstatusinput;

  receiveMeasureButtonStatusInput() {
    this.searchsidebarServe.getMeasureButtonStatusInput$.subscribe((measurebuttonstatusinput) => { this.measurebuttonstatusinput = measurebuttonstatusinput; });
  }

  receiveAddressButtonStatusInput() {
    this.searchsidebarServe.getAddressButtonStatusInput$.subscribe((addressbuttonstatusinput) => { this.addressbuttonstatusinput = addressbuttonstatusinput; });
  }

  addMapEventListeners() {
    var homeComponent = this
    this.map.on('click', function(evt) {
      document.body.style.cursor = "";
      if (measureFeat.length > 3) {
        zoomSource.clear();
        measureFeat = [];
      }
      if (homeComponent.measurebuttonstatusinput !== true) {
        zoomSource.clear();
        zoomLayer.setVisible(false);
      }
      document.getElementById("map").style.opacity = "1.0";
      //if clicked and not feature, close sidenav and reset style of rbs
      homeComponent.sidenav.close();
      //for each feature get lat long and display in sidenav
      this.forEachFeatureAtPixel(evt["pixel"], function(feature, layer) {
        if (homeComponent.measurebuttonstatusinput == true) {
          var message = homeComponent.mapFunctions.measureFeatures(feature, zoomLayer, newFeat, measureFeat);
          if (measureFeat.length == 4) {
            homeComponent.notifyServ.showInfo("Distance between = " + message)
            setTimeout(() => {
              zoomSource.clear()
            }, 3000);
          }
        }
        else {
          zoomSource.clear();
          zoomLayer.setVisible(false);
          if ((feature.getProperties.features !== undefined) && (homeComponent.measurebuttonstatusinput !== true &&homeComponent.addressbuttonstatusinput !== true)) {
            homeComponent.findFeature(feature);
          }
        }
      })
    }) // end of click event listener

    // user zooming event listener
    this.map.on('moveend', function(e) {
      //get zoom, also called at initial map load
      zoom = this.getView().getZoom()
      if (zoom < 9) {
        clusterSource.setDistance(50);
      }
      else if (zoom >= 9) {
        clusterSource.setDistance(30);
      }
      else if (zoom >= 12) {
        clusterSource.setDistance(7);
      }
      else if (zoom > 20) {
        clusterSource.setDistance(0);
      }
    }) // end of user zooming event listener

    // whent he user hovers over a feature the cursor becomes a pointer
    this.map.on('pointermove', function(evt) {
      var hit = this.forEachFeatureAtPixel(evt["pixel"], function(feature, layer) {
        return true;
      });
      if (hit) {
        document.body.style.cursor = "pointer";
      }
      else {
        document.body.style.cursor = "";
      }
    });



    this.map.on('click', function(evt: any) {
      this.forEachFeatureAtPixel(evt["pixel"], function(feature: ol.Feature) {
        if ((feature.getProperties().features !== undefined) && (homeComponent.measurebuttonstatusinput !== true && homeComponent.addressbuttonstatusinput !== true)) {
          homeComponent.findFeature(feature);
        }
      });
      if (homeComponent.addressbuttonstatusinput == true) {
        var coOrds = homeComponent.getCoordsFromClick(evt);
        homeComponent.mapFunctions.reverseGeocoding(coOrds[0], coOrds[1], false, homeComponent.notifyServ);
        homeComponent.addFeat(coOrds[0], coOrds[1])
      }
    });

  }

  getCoordsFromClick(evt) {
    var coords = ol.proj.toLonLat(evt.coordinate);
    return coords;
  }

  // open a feature when it's selected
  findFeature(feature) {
    var coord = feature.getGeometry().getCoordinates();
    coord = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
    var newStyle = new ol.style.Style({
      image: new ol.style.Icon({
        src: '../assets/images/inverted_tower2.png',
        scale: .1,
      })
    })


    //if not cluster, open sidenav and highlight clicked rbs
    if (!this.mapFunctions.isCluster(feature) && feature.getStyle() !== "centreStyle") {
      zoomLayer.setVisible(true);
      newFeat = new ol.Feature({
        geometry: new ol.geom.Point(
          ol.proj.fromLonLat(
            [coord[0], coord[1]])
        )
      })
      // change it's style to highlight it
      newFeat.setStyle(newStyle);
      zoomSource.addFeature(newFeat);
      coord[0] = (Math.round(coord[0] * 10000000) / 10000000);
      coord[1] = (Math.round(coord[1] * 10000000) / 10000000);
      this.mapFunctions.reverseGeocoding(coord[0], coord[1], true)

      for (var i = 0; i < this.rbsMap.rbss.length; i++) {
        var lonJson = (Math.round(parseFloat(this.rbsMap.rbss[i].longitude) * 10000000) / 10000000);
        var latJson = (Math.round(parseFloat(this.rbsMap.rbss[i].latitude) * 10000000) / 10000000);
        var request = "";
        if (latJson == coord[1] && lonJson == coord[0]) {
          this.sidenav.open();
          this.rbs = this.rbsMap.rbss[i];
          for (var g = 0; g < this.rbsMap.rbss[i].geohashes.length; g++) {
            if (g == (this.rbsMap.rbss[i].geohashes.length - 1)) {
              request = request + this.rbsMap.rbss[i].geohashes[g];
            }
            else {
              request = request + this.rbsMap.rbss[i].geohashes[g] + ",";
            }
          }
          // get the rbs' cell info after finding it
          this.data.getFullCellInfo(request).then((cellArray: Cell[]) => {
            // send that data off to the cell request
            var cgis: string[] = [];
            for (var i = 0; i < cellArray.length; i++) {
              cgis.push(cellArray[i].cgi);
            }
            this.cgiList = cgis;
            this.cellList = cellArray;
            this.setCell = cellArray[0];
            this.table.setCells(cellArray);
          })
          // fade the map to focus on the sidenavs
          document.getElementById("map").style.opacity = "0.5"
          break;
        }
      }
    }
    //if cluster, zoom in to split cluster up
    else {
      this.map.getView().setCenter(ol.proj.transform([coord[0], coord[1]], 'EPSG:4326', 'EPSG:3857'));
      this.map.getView().animate({
        zoom: zoom + 3,
        duration: 400
      });
    }
  }

  //function to move map, used by Automove object

  addFeat(lon, lat) {
    zoomSource.clear();
    zoomLayer.setVisible(true);
    var style1 = new ol.style.Style({
      image: new ol.style.Icon({
        src: '../assets/images/pin.png',
        scale: .06,
      })
    })
    var Feat = new ol.Feature({
      geometry: new ol.geom.Point(
        ol.proj.fromLonLat(
          [parseFloat(lon), parseFloat(lat)])
      )
    })
    Feat.setStyle(style1);
    zoomSource.addFeature(Feat);
    setTimeout(() => {
      zoomSource.clear();
    }, 3500);
  }
}
