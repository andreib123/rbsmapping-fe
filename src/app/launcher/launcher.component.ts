import { Component, OnInit } from '@angular/core';
import { MapFunctionalityService } from '../map-functionality.service';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { RbsMap } from '../model/rbs-map.model';
var colourb: boolean = false;
@Component({
  selector: 'app-launcher',
  templateUrl: './launcher.component.html',
  styleUrls: ['./launcher.component.css']
})

export class LauncherComponent implements OnInit {
  constructor(private _router: Router, private mapFunctions: MapFunctionalityService, private data: DataService) { }
  opened = false;

  ngOnInit() {
    this.data.getFullRbsMapInfo().then((rbsMap: RbsMap) => {
      this.mapFunctions.rbsMap = rbsMap
      console.log("mapFunctions.rbsMap is defined")
    })
  }

  showSettings() {
    console.log("click")
  }

  colourBlind() {
    colourb = !colourb;
    this.mapFunctions.setColourBlind(colourb);
    this.opened = false;
  }

  launch(road: Event) {
    if (road.srcElement.innerHTML == "Road Map") {
      this.mapFunctions.setRoad(true);
    }
    else {
      this.mapFunctions.setRoad(false);
    }
    this._router.navigate(["/map"]);
  }
}
