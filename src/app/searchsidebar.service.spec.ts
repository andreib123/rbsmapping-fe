import { TestBed } from '@angular/core/testing';

import { SearchsidebarService } from './searchsidebar.service';

describe('SearchsidebarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchsidebarService = TestBed.get(SearchsidebarService);
    expect(service).toBeTruthy();
  });
});
