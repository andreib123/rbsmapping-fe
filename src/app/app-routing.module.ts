import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TableComponent } from './table/table.component';
import { LauncherComponent } from './launcher/launcher.component';
import { BackgroundImageResolver } from './BackgroundImage/bg-image.resolver';
const routes: Routes = [
  {
    // route to home page.
    path: '',
    component: LauncherComponent,
    resolve: {
      background: BackgroundImageResolver
    }
  },
  {
    path: 'map',
    component: HomeComponent
  },
  { path: 'table', component: TableComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers:[BackgroundImageResolver]
})
export class AppRoutingModule { }
