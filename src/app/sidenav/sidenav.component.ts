import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TableComponent } from '../table/table.component';
import { MatDialog } from '@angular/material';
import { Cell } from '../model/cell.model';
import { RbsForInitView } from '../model/rbs-for-init-view.model';
import { ExcelService } from "../excel.service";

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})

export class SidenavComponent implements OnInit {
  constructor(private dialog: MatDialog, private excelService: ExcelService) { }
  @Input() selectedRbs: RbsForInitView;
  @Input() cells: Cell[];
  @Input() cgis: string[];
  @Output() closeMethod = new EventEmitter();
  @Output() getCell = new EventEmitter<Cell>();

  ngOnInit() {
  }

  // close the sidenav and endnav
  close() {
    this.closeMethod.emit();
  }

  // call to the home component to change the cell displayed in the endnav component
  changeCellInfo(deviceValue: any) {
    for (var i = 0; i < this.cells.length; i++) {
      if (this.cells[i].cgi == deviceValue) {
        this.getCell.emit(this.cells[i]);
      }
    }
  }

  // open the dialog box that contains this rbs' data in a table
  openDialog(): void {
    this.dialog.open(TableComponent, {
    });
  }

  // call the excelservice to export this rbs' data as an excel file
  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.cells, 'sample')
  }
}
