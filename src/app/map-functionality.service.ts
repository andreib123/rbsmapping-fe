import { Injectable } from '@angular/core';
import { RbsMap } from './model/rbs-map.model';
import * as ol from "openlayers";
import { ExcelService } from "./excel.service";
import { DataService } from "./data.service";
import { Cell } from './model/cell.model';
import { ToastrNotificationService } from './toastr-notification.service';
var road: boolean;
var colourBlind: boolean;

@Injectable({
  providedIn: 'root'
})
export class MapFunctionalityService {
  exportRequest: string = "";
  rbsMap: RbsMap
  filteredVectorLayer: ol.layer.Vector;

  constructor(private excelService: ExcelService, private data: DataService) { }

  setRoad(isRoad: boolean) {
    road = isRoad;
  }

  getRoad(): Boolean {
    return road;
  }

  setFilteredVectorLayer(filteredVectorLayer: ol.layer.Vector) {
    this.filteredVectorLayer = filteredVectorLayer;
  }

  getDistanceFromLatLonInKm(Coord: any[]): number {
    var lon1 = Coord[0];
    var lat1 = Coord[1];
    var lon2 = Coord[2];
    var lat2 = Coord[3];
    var R = 6371; // Radius of the earth in km
    var dLat = (lat2 - lat1) * (Math.PI / 180);  // deg2rad below
    var dLon = (lon2 - lon1) * (Math.PI / 180);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(lat1 * (Math.PI / 180)) * Math.cos(lat2 * (Math.PI / 180)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = Math.round(R * c * 100) / 100; // Distance in km
    return d;
  }

  getCoordsFromClick(evt) {
    console.log(evt.pixel);
    var coords = ol.proj.toLonLat(evt.coordinate);
    console.log(coords);
    return coords;
  }

  reverseGeocoding(lon, lat, sidenav, notify?) {
    lon = (Math.round(lon * 10000) / 10000)
    lat = (Math.round(lat * 10000) / 10000)
    var url = 'http://nominatim.openstreetmap.org/reverse?format=json&lon=' + lon + '&lat=' + lat
    fetch(url).then(function(response) {
      return response.json();
    }).then(function(json) {
      if (sidenav == true) {
        var address = document.getElementById("address");
        address.innerText = "Address: " + json.display_name;
      }
      else {
        // alert(json.display_name)
        var add1 = "Coordinates: " + lon + ", " + lat + " " + "\n" + "Address: " + json.display_name;
        notify.showInfo(add1);
      }
    })
  }

  isCluster(feature: ol.Feature) {
    if (!feature || !feature.get('features')) {
      return false;
    }
    return feature.get('features').length > 1;
  }

  measureFeatures(feature, zoomLayer, newFeat, measureFeat): string {
    var coord = feature.getGeometry().getCoordinates();
    coord = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
    var lon = coord[0];
    var lat = coord[1];
    var newStyle = new ol.style.Style({
      image: new ol.style.Icon({
        //src: 'https://upload.wikimedia.org/wikipedia/commons/2/2a/Dot.jpg'
        src: '../assets/images/inverted_tower2.png',
        scale: .1,
      })
    })
    if (!this.isCluster(feature)) {
      zoomLayer.setVisible(true);
      newFeat = new ol.Feature({
        geometry: new ol.geom.Point(
          ol.proj.fromLonLat(
            [lon, lat])
        )
      })
      newFeat.setStyle(newStyle);
      zoomLayer.getSource().addFeature(newFeat);
    }

    if (measureFeat.length < 2) {
      measureFeat[0] = lon;
      measureFeat[1] = lat;
    }
    else if (measureFeat.length == 2) {
      measureFeat[2] = lon;
      measureFeat[3] = lat;
    }
    else {
      measureFeat = [];
      zoomLayer.getSource().clear();
    }

    if (measureFeat.length == 4) {
      var distance = this.getDistanceFromLatLonInKm(measureFeat).toString() + "km";
    }
    console.log("distance: " + distance);
    if (distance != null) {
      return distance;
    }
    else {
      return "";
    }
  }

  exportFiltered(notifyServ: ToastrNotificationService, rbsmap: RbsMap) {
    this.rbsMap = rbsmap;
    this.exportRequest = "";
    if (!(this.filteredVectorLayer == undefined)) {
      notifyServ.showInfo("export starting")
      var features = this.filteredVectorLayer.getSource().getFeatures()
      console.log(features.length);
      for (var i = 0; i < features.length; i++) {
        this.isClusterExport(features, i, notifyServ);
      }
    }
    else {
      notifyServ.showError("No results filtered");
    }
  }

  searchFeature(featureF: ol.Feature, clusterEnd: boolean, notifyServ: { showSuccess: (arg0: string) => void; showError: (arg0: string) => void; }) {
    var feature: ol.geom.Point = <ol.geom.Point>featureF.getGeometry();
    var coordf: ol.Coordinate = feature.getCoordinates();
    coordf = ol.proj.transform(coordf, 'EPSG:3857', 'EPSG:4326');
    coordf[0] = Math.round(coordf[0] * 10000) / 10000;
    coordf[1] = Math.round(coordf[1] * 10000) / 10000;
    // goes through each rbs to find the enodeBID
    for (var j = 0; j < this.rbsMap.rbss.length; j++) {
      if (this.rbsMap.rbss[j].longitude == coordf[0].toString() && this.rbsMap.rbss[j].latitude == coordf[1].toString()) {
        // gets the geohashes found on that rbs
        for (var k = 0; k < this.rbsMap.rbss[j].geohashes.length; k++) {
          // checks if its the end of a cluster
          if (k == this.rbsMap.rbss[j].geohashes.length - 1 && clusterEnd) {
            // filters the number of geohashes to prevent requests that are too big
            //!!!!!there is an odd error where data that is of the right size will somehow be called as being too big of a request
            if ((this.exportRequest.match(/,/g) || []).length <= 1249) {
              this.exportRequest = this.exportRequest + this.rbsMap.rbss[j].geohashes[k];
              this.data.getFullCellInfo(this.exportRequest).then((cells: Cell[]) => {
                console.log(this.exportRequest);
                // calls export service to create excel file
                this.excelService.exportAsExcelFile(cells, 'sample');
                notifyServ.showSuccess("export finished")
              });
            }
            else {
              console.log((this.exportRequest.match(/,/g) || []).length);
              notifyServ.showError("request is too big");
            }
          }
          else {
            this.exportRequest = this.exportRequest + this.rbsMap.rbss[j].geohashes[k] + ",";
          }
        }
      }
    }
  }

  isClusterExport(features: ol.Feature[], i: number, notifyServ: any) {
    var clusterEnd = false;
    if (!features[i] || !features[i].get('features')) {
      // checks if final feature
      if (i == features.length - 1) {
        clusterEnd = true;
      }
      this.searchFeature(features[i], clusterEnd, notifyServ)
    }
    else if (features[i] || features[i].get('features')) {
      // checks if cluster then goes through each one, if final feature in a cluster it lets the search function know
      for (var f = 0; f < features[i].get('features').length; f++) {
        if (i == features.length - 1 && f == features[i].get('features').length - 1) {
          clusterEnd = true;
        }
        this.searchFeature(features[i].get('features')[f], clusterEnd, notifyServ)
      }
    }
  }

  setColourBlind(val) {
    colourBlind = val;
  }

  getColourBlind() {
    return colourBlind;
  }
}
