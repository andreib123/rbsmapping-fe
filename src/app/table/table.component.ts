import { Component, OnInit, Input } from '@angular/core';
import { TableService } from '../table.service';
import { Cell } from '../model/cell.model';

@Component({
  selector: 'table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  constructor(private table: TableService) {
  }
  tableArray: Cell[];
  // set the different columns that will be displayed on the table
  displayedColumns: string[] = ['geohash', 'market', 'region', 'cellType', 'cgi', 'azimuth', 'freqBand', 'tac', 'latitude', 'longitude', 'retcapable', 'enodeBID'];
  ngOnInit() {
    // set the info for the table
    this.tableArray = this.table.getCells();
  }
}
