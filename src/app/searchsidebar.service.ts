import { Injectable } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav, MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
var searchsidebarElement;
@Injectable({
  providedIn: 'root'
})
export class SearchsidebarService {

  getCoordsInput$: Observable<any>;
  private coordsInputSubject = new Subject();
  getEnodeBIDInput$: Observable<any>;
  private enodeBIDInputSubject = new Subject();
  getCGIInput$: Observable<any>;
  private cgiInputSubject = new Subject();

  getStateInput$: Observable<any>;
  private stateInputSubject = new Subject();
  getCityInput$: Observable<any>;
  private cityInputSubject = new Subject();

  getNumCellsInput$: Observable<any>;
  private numCellsInputSubject = new Subject();
  getNumCellCriteria$: Observable<any>;
  private numCellCriteriaSubject = new Subject();

  getGoClickEvent$: Observable<any>;
  private goClickEventSubject = new Subject();

  getMeasureButtonStatusInput$: Observable<any>;
  private measureButtonStatusInputSubject = new Subject();

  getEnode1Input$: Observable<any>;
  private  enode1InputSubject = new Subject();

  getEnode2Input$: Observable<any>;
  private enode2InputSubject = new Subject();

  getLocSelectButtonStatusInput$: Observable<any>;
  private locSelectButtonStatusInputSubject = new Subject();
  getLocRadiusInput2$: Observable<any>;
  private locRadiusInput1Subject = new Subject();
  getLocRadiusInput1$: Observable<any>;
  private locRadiusInput2Subject = new Subject();

  getAddressButtonStatusInput$: Observable<any>;
  private addressButtonStatusInputSubject = new Subject();
  constructor() {
    this.getCoordsInput$ = this.coordsInputSubject.asObservable();
    this.getStateInput$ = this.stateInputSubject.asObservable();
    this.getCityInput$ = this.cityInputSubject.asObservable();
    this.getMeasureButtonStatusInput$ = this.measureButtonStatusInputSubject.asObservable();
    this.getEnode1Input$= this.enode1InputSubject.asObservable();
    this.getEnode2Input$= this.enode2InputSubject.asObservable();
    this.getLocSelectButtonStatusInput$ = this.locSelectButtonStatusInputSubject.asObservable();
    this.getAddressButtonStatusInput$ = this.addressButtonStatusInputSubject.asObservable();
  }

  setSearchsidebar(searchsidebar){
    searchsidebarElement = searchsidebar;
  }

  getSearchsidebarElement() {
    return searchsidebarElement;
  }

  getCoordsInput(input){
    console.log(input);
    this.coordsInputSubject.next(input);
  }

  getStateInput(input){
    console.log(input);
    this.stateInputSubject.next(input);

  }

  getCityInput(input){
    console.log(input);
    this.cityInputSubject.next(input);
  }

  getGoClickEvent(event){
    console.log(event);
    this.goClickEventSubject.next(event);
  }

  getMeasureButtonStatus(input){
    console.log(input);
    this.measureButtonStatusInputSubject.next(input);
  }

  getAddressButtonStatus(input){
    console.log("service add: "+input);
    this.addressButtonStatusInputSubject.next(input);
  }

  getEnode1Input(input){
    console.log(input);
    this.enode1InputSubject.next(input);

  }
  getEnode2Input(input){
    console.log("Service has recieved:"+input);
    this.enode2InputSubject.next(input);
  }

  getLocSelectButtonStatusInput(input){
    console.log(input);
    this.locSelectButtonStatusInputSubject.next(input);
  }
}
