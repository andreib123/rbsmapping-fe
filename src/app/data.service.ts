import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';
import { RbsMap } from './model/rbs-map.model';
import { Cell } from './model/cell.model';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

// get all the rbss from the database and store them in an rbsmap
  getFullRbsMapInfo(): Promise<RbsMap> {
    var url = environment.rbsInteractiveMappingServiceBackend + 'objects/view/init'

    let headers1 = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/' + environment.acceptType,
    })

    console.log("DataService.getFullRbsMapInfo is being called")

    return this.http.get<RbsMap>(url, { headers: headers1, responseType: environment.acceptType as 'json' }).toPromise()
  }

  // return all data for cells located on all geohashes specified
  getFullCellInfo(geohashes: string): Promise<Cell[]> {
    var url = environment.rbsInteractiveMappingServiceBackend + 'objects/view/rbs?geohash=' + geohashes;

    let headers1 = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/' + environment.acceptType,
    })

    console.log("DataService.getFullCellInfo is being called")

    return this.http.get<Cell[]>(url, { headers: headers1, responseType: environment.acceptType as 'json' }).toPromise()
  }
}
