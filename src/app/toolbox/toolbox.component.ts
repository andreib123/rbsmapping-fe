import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { RbsMap } from '../model/rbs-map.model';
import { Cell } from '../model/cell.model';
import { ToastrNotificationService } from '../toastr-notification.service';
import { SearchsidebarService } from '../searchsidebar.service';
import * as ol from 'openlayers'
import { FeatureSetupService } from '../feature-setup.service'
import { MapFunctionalityService } from '../map-functionality.service';
import { MatButtonToggle, MatRadioGroup } from '@angular/material';
import { DataService } from '../data.service'

@Component({
  selector: 'app-toolbox',
  templateUrl: './toolbox.component.html',
  styleUrls: ['./toolbox.component.css']
})
export class ToolboxComponent implements OnInit {

  @Input() RbsMap: RbsMap;
  @Input() map: ol.Map;
  @Input() source: ol.source.Vector;
  @Input() notifyServ: ToastrNotificationService;
  @Input() featureSetup: FeatureSetupService;
  @Input() colourBlindMode: boolean;
  multifilterinput;
  regioninput;
  multifiltertype;
  numcellsinput;
  criteriatoggle;
  radiussearchinput1;
  radiussearchinput2;
  locselect;
  enode1input;
  enode2input;
  measurebuttonstatus;
  addressbuttonstatus;
  opt1;
  opt2;
  opt3;
  filteredArray = [];
  regionautocompleteoptions = [];
  filteredVectorLayer;
  searchJson: any[] | { display_name: { includes: (arg0: string) => void; }; }[];
  radio: boolean = false;
  region: boolean = false;
  exportReady: Boolean = false;
  @ViewChild('selectLocation')
  public selectLoc: MatButtonToggle;
  @ViewChild('radioButtons')
  public radioBtns: MatRadioGroup;


  constructor(private mapFunctions: MapFunctionalityService, private searchsb: SearchsidebarService, private data: DataService) { }

  ngOnInit() {
    this.setRadiusCentre();
  }

  radioChange() {
    this.multifilterinput = this.radioBtns.value;
  }

  // checks the multifilter to see if it's set to ret capable, if it is it switches the input to a radio button group
  checkRadio() {
    if (this.multifiltertype == "RET Capability") {
      this.radio = true;
    }
    else {
      this.radio = false;
    }
    this.multifilterinput = undefined;
  }

  checkRegion() {
    if (this.multifiltertype == "Region") {
      this.region = true;
      for(let i = 0; i < this.RbsMap.rbss.length; i++){
        let regiontest = this.RbsMap.rbss[i].region;
        if(this.regionautocompleteoptions.includes(regiontest) == false){
          this.regionautocompleteoptions.push(regiontest)
        }
      }
    }
    else {
      this.region = false;
      this.multifilterinput=undefined;
    }
  }

  // if the radius button is clicked it checks to see if the num cell input has a value in it so it knows which filters to call
  radiusButton() {
    if (this.numcellsinput == undefined || this.numcellsinput == "") {
      this.justRadiusSearch();
    }
    else {
      this.filterNumCells();
    }
  }

  ready() {
    this.exportReady = true;
  }

  multiBtnPushed() {
    if (!(this.multifilterinput == undefined || this.multifilterinput == "")||!(this.regioninput == undefined || this.regioninput == "")) {
      if (!(this.radiussearchinput2 == undefined || this.radiussearchinput2 == "")
        && (this.numcellsinput == undefined || this.numcellsinput == "")) { this.justRadiusSearch() }
      else if (!(this.numcellsinput == undefined || this.numcellsinput == "")) {
        this.filterNumCells();
      }
      else {
        this.filteredArray = [];
        for (var i = 0; i < this.RbsMap.rbss.length; i++) {
          this.filterMulti(i);
        }
        if(this.multifiltertype == "TAC"){
          if(this.filteredArray.length >= 1){
            this.notifyServ.showInfo(this.filteredArray.length+" RBS found in TAC: "+this.multifilterinput);
          }
          else{
            this.notifyServ.showError("Invalid TAC entered")
          }
        }
        else if(this.multifiltertype == "RET Capability"){
          if((this.filteredArray.length >= 1) && (this.multifilterinput == 1)){
            this.notifyServ.showInfo(this.filteredArray.length+" RBS found with RET capability");
          }
          else if((this.filteredArray.length >= 1) && (this.multifilterinput == 0)){
            this.notifyServ.showInfo(this.filteredArray.length+" RBS found without RET capability");
          }
          else{
            this.notifyServ.showError("Invalid RET capability entered")
          }
        }
        else if(this.multifiltertype == "Frequency Band"){
          if(this.filteredArray.length >= 1){
            this.notifyServ.showInfo(this.filteredArray.length+" RBS found on frequency band "+this.multifilterinput);
          }
          else{
            this.notifyServ.showError("Invalid or unused frequency band entered")
          }
        }
        else if(this.multifiltertype == "Region"){
          if(this.filteredArray.length >= 1){
            this.notifyServ.showInfo(this.filteredArray.length+" RBS found in "+this.regioninput+" region");
          }
          else{
            this.notifyServ.showError("Invalid region entered")
          }
        }
        this.resetLayers(0);
        this.plotFilteredFeatures(this.filteredArray);
      }
    }
  }

  // runs the rbsMap through a filter depending on which filter type is chosen
  filterMulti(i: number) {

    if (this.multifiltertype == "Region") {
      if (this.RbsMap.rbss[i].region.toLowerCase() == this.regioninput.toLowerCase()) {
        this.pushToArray(i);
      }
    }
    if (this.multifiltertype == "TAC") {
      if (this.RbsMap.rbss[i].tac == this.multifilterinput) {
        this.pushToArray(i);
      }
    }
    if (this.multifiltertype == "RET Capability") {
      if (this.RbsMap.rbss[i].retcapable == this.multifilterinput) {
        this.pushToArray(i);
      }
    }
    if (this.multifiltertype == "Frequency Band") {
      if (this.RbsMap.rbss[i].freqBand == this.multifilterinput) {
        this.pushToArray(i);
      }
    }
  }

  filterNumCells() {
    this.filteredArray = [];
    var radius1 = this.radiussearchinput1;
    var radius2 = this.radiussearchinput2;
    var numSearch = this.numcellsinput;
    var searchCriteria = this.criteriatoggle;
    for (var i = 0; i < this.RbsMap.rbss.length; i++) {
      // filters numCells >= chosen value
      if (searchCriteria == "greater") {
        if ((this.RbsMap.rbss[i].numCells == numSearch) || (this.RbsMap.rbss[i].numCells > numSearch)) {
          this.filter(radius1, radius2, i);
        }
      }
      // filters numCells = chosen value
      else if ((searchCriteria == "equal") || (searchCriteria == undefined)) {
        if (this.RbsMap.rbss[i].numCells == numSearch) {
          this.filter(radius1, radius2, i);
        }
      }
      // filters numCells <= chosen value
      else if (searchCriteria == "less") {
        if ((this.RbsMap.rbss[i].numCells == numSearch) || (this.RbsMap.rbss[i].numCells < numSearch)) {
          this.filter(radius1, radius2, i);
        }
      }
    }
    // show how many rbss meet criteria
    this.notifyServ.showInfo("Filtering RBS by criteria: \n Number of RBS found: " + this.filteredArray.length);
    this.resetLayers(0);
    this.plotFilteredFeatures(this.filteredArray);
    this.selectLoc.checked = false;
  }

  filter(radius1, radius2, i) {
    // checks radius input
    if (radius1 == undefined || radius1 == "" && radius2 == undefined || radius2 == "") {
      // checks multi filter if empty push to array, if not filter through the filter multi array
      if (this.multifilterinput == undefined || this.multifilterinput == "") { this.pushToArray(i); }
      else { this.filterMulti(i) }
    }
    // if radius inupt, set the location
    else {
      radius1[2] = this.RbsMap.rbss[i].longitude;
      radius1[3] = this.RbsMap.rbss[i].latitude;
      // find rbss in that location
      if (this.mapFunctions.getDistanceFromLatLonInKm(radius1) <= radius2) {
        // checks multi filter if empty push to array, if not filter through the filter multi array
        if (this.multifilterinput == undefined || this.multifilterinput == "") { this.pushToArray(i); }
        else { this.filterMulti(i) }
      }
    }
  }

  // pushes all filtered features to the filtered array
  pushToArray(i) {
    this.filteredArray.push(
      new ol.Feature({
        geometry: new ol.geom.Point(
          ol.proj.fromLonLat(
            [parseFloat(this.RbsMap.rbss[i].longitude),
            parseFloat(this.RbsMap.rbss[i].latitude)]
          )
        ),
        name: parseFloat(this.RbsMap.rbss[i].numCells)
      })
    )
  }

  // set the radius marker on the part of the map that the user clicks
  setRadiusCentre() {
    this.map.on('click', (evt: any) => {
      if (this.locselect == true) {
        this.radiussearchinput1 = this.getCoordsFromClick(evt);
        this.plotRadius(this.radiussearchinput1, this.map);
      }
    });

  }

  // Get the coordinates from where the user clicks on the map
  getCoordsFromClick(evt) {
    var coords = ol.proj.toLonLat(evt.coordinate);
    return coords;
  }

  // search based on the radius
  justRadiusSearch() {
    var textinputregex = new RegExp("[a-zA-Z]+");
    this.filteredArray = [];
    var coord = [];
    if (textinputregex.test(this.radiussearchinput1) == true) {
      coord = this.coordsFromJSON();
    }
    else {
      if (this.locselect == true) {
        coord = this.radiussearchinput1;
      }
      else if (this.locselect == false) {
        coord = this.radiussearchinput1.split(",");
      }
    }
    var rad = this.radiussearchinput2;
    for (let i = 0; i < this.RbsMap.rbss.length; i++) {
      coord[2] = this.RbsMap.rbss[i].longitude;
      coord[3] = this.RbsMap.rbss[i].latitude;
      if (this.mapFunctions.getDistanceFromLatLonInKm(coord) <= rad) {
        // check if multifilter has a value if not, push to array, if it does send through that filter method
        if (this.multifilterinput == undefined || this.multifilterinput == "") { this.pushToArray(i); }
        else { this.filterMulti(i) }
      }
    }
    if (this.map.getLayers().getArray().length > 4) {
      this.resetLayers(1);
    }
    this.plotFilteredFeatures(this.filteredArray);
    this.moveTo(coord, this.radiussearchinput2);
    this.notifyServ.showInfo(this.filteredArray.length + " RBS found within " + rad + "km radius of selection");
    this.selectLoc.checked = false;
  }

  plotRadius(coord: number[], map: ol.Map) {
    if (map.getLayers().getArray().length == 4) {
      var radiusLayerSource = new ol.source.Vector({})
      var radiusLayer = new ol.layer.Vector({
        source: radiusLayerSource
      })
      map.addLayer(radiusLayer);
    }
    else if (map.getLayers().getArray().length > 4) {
      var radiusLayer = <ol.layer.Vector>map.getLayers().getArray()[4];
      var radiusLayerSource = radiusLayer.getSource();
      radiusLayerSource.clear();
    }
    let centreStyle = new ol.style.Style({
      image: new ol.style.Icon({
        //src: 'https://upload.wikimedia.org/wikipedia/commons/2/2a/Dot.jpg'
        src: '../assets/images/location.png',
        scale: 0.5,
      })
    })
    let centre = new ol.Feature({
      geometry: new ol.geom.Point(ol.proj.fromLonLat([coord[0], coord[1]])),
    })
    centre.setStyle(centreStyle);
    radiusLayerSource.addFeature(centre);
  }

  plotFilteredFeatures(filteredFeatures: any[]) {
    var filterComponent = this;
    let vectorLayer = this.map.getLayers().getArray()[2];
    vectorLayer.setVisible(false);
    this.filteredVectorLayer = new ol.layer.Vector({
      // clusterSource
      source: new ol.source.Cluster({
        distance: 15, // cluster size
        // vectorSource
        source: new ol.source.Vector({
          features: filteredFeatures
        })
      }),
      style: function(feature) {
        var style = filterComponent.featureSetup.setStyle(feature, filterComponent.colourBlindMode)
        return style;
      }
    })
    this.map.addLayer(this.filteredVectorLayer);
    this.filteredVectorLayer.setVisible(true);
    this.mapFunctions.setFilteredVectorLayer(this.filteredVectorLayer);
  }

  moveTo(coord, radius){
    var duration = 1500;
    var zoomout = 4;
    var zoomVal;
    var durationOut = 700;
    var currLoc = ol.proj.transform(this.map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
    var lon: number = Math.round(coord[0] * 10000) / 10000
    var lat: number = Math.round(coord[1] * 10000) / 10000;
    var distance = this.mapFunctions.getDistanceFromLatLonInKm([currLoc[0], currLoc[1], lon, lat])
    var duration = 2000;
    var duration1 = 1500;

    if(radius >= 1 && radius <=50){
      zoomVal = 9.5;
    }
    if(radius >= 51 && radius <=100){
      zoomVal = 8.5;
    }
    if(radius >= 101 && radius <=250){
      zoomVal = 7.5;
    }
    if(radius >= 251 && radius <=550){
      zoomVal = 6.5;
    }
    if(radius >= 551 && radius <=1500){
      zoomVal = 5.5;
    }
    if(radius >=1501){
      zoomVal = 4.5;
    }
    this.map.getView().animate({
      zoom: zoomout,
      duration: durationOut,
    },
      {
        center: ol.proj.fromLonLat([lon, lat]),
        duration: duration1,
      }, {
        zoom: zoomVal,
        duration: duration,
      });


  }

  resetLayers(arg) {
    this.exportReady = false;
    var numberlayers = this.map.getLayers().getArray().length;
    if (arg == 0) {
      for (let i = numberlayers; i > 3; i--) {
        let currentLayer = <ol.layer.Vector>this.map.getLayers().getArray()[i];
        this.map.removeLayer(currentLayer);
      }
    }
    if (arg == 1) {
      for (let i = numberlayers; i > 4; i--) {
        let currentLayer = <ol.layer.Vector>this.map.getLayers().getArray()[i];
        this.map.removeLayer(currentLayer);

      }
    }
    if (arg == 2) {
      for (let i = numberlayers; i > 3; i--) {
        let currentLayer = <ol.layer.Vector>this.map.getLayers().getArray()[i];
        this.map.removeLayer(currentLayer);
      }
      this.map.getLayers().getArray()[2].setVisible(true);
    }

    if(arg == 3){
      let currentLayer = <ol.layer.Vector>this.map.getLayers().getArray()[4];
      this.map.removeLayer(currentLayer);
    }
  }

  sendMeasureButtonStatus() {
    var measurebuttonstatusinput = this.measurebuttonstatus;
    this.searchsb.getMeasureButtonStatus(measurebuttonstatusinput)
  }

  sendAddressButtonStatus() {
    var addressbuttonstatusinput = this.addressbuttonstatus;
    this.searchsb.getAddressButtonStatus(addressbuttonstatusinput)
  }

  sendEnode1Input() {
    this.searchsb.getEnode1Input(this.enode1input);

  }

  sendEnode2Input() {
    this.searchsb.getEnode2Input(this.enode2input);
  }

  export() {
    // export the data from the filtered layer to an excel file
    if (this.exportReady) {
      this.mapFunctions.exportFiltered(this.notifyServ, this.RbsMap);
    }
    else {
      this.notifyServ.showError("Nothing filtered");
    }
  }

  resetSearch() {
    this.numcellsinput = undefined;
    this.radiussearchinput1 = undefined;
    this.radiussearchinput2 = undefined;
    this.criteriatoggle = undefined;
    this.enode1input = undefined;
    this.enode2input = undefined;
    this.locselect = false;
    this.multifilterinput = undefined;
    this.multifiltertype = undefined;
    this.regioninput = undefined;
  }

  enodeMeasure() {
    var enode1= (<HTMLInputElement>document.getElementById("measureId")).value;
   var enode2= (<HTMLInputElement>document.getElementById("measureId1")).value;
    var coOrd = [];
    var index = 0;
    var homeComponent = this;
    var rbsMap = homeComponent.RbsMap;
    let regexp = new RegExp('^[0-9]{3}[-][0-9]{3}[-][0-9]{0,6}[-][0-9]{0,3}$');
    var searchRequest = "";
    if (typeof enode1 !== 'undefined' && typeof enode2 !== 'undefined') {
      var searchInput = [enode1, enode2]
      for (var x = 0; x < searchInput.length; x++) {
        if (regexp.test(searchInput[x])) {
          var split = searchInput[x].split('-');
          searchRequest = split[2];
          if (searchRequest.length < 6) {
            var count = 6 - searchRequest.length;
            for (var i = 1; i <= count; i++) {
              searchRequest = '0' + searchRequest;
            }
          }
        }
        else {
          searchRequest = searchInput[x];
        }
        for (var i = 0; i < rbsMap.rbss.length; i++) {
          if (rbsMap.rbss[i].eNodeBID == searchRequest) {
            var lonJSON = (Math.round(parseFloat(rbsMap.rbss[i].longitude) * 10000) / 10000);
            var latJSON = (Math.round(parseFloat(rbsMap.rbss[i].latitude) * 10000) / 10000);
          }
        }
        coOrd[index] = lonJSON;
        index++;
        coOrd[index] = latJSON;
        index++;
      }
      var distance = this.mapFunctions.getDistanceFromLatLonInKm(coOrd);
      if (distance > 0) {
        this.notifyServ.showInfo("There is " + distance + " KM between these base stations");
      }
      else {
        this.notifyServ.showError("eNodeBID not found")
      }
      index++;
    }
  }

  radiusSearchAutocomplete() {
    var input = this.radiussearchinput1;
    var usa = ",USA";
    var toolboxComponent = this;
    if (input.includes("usa") || input.includes("United States of America") || input.includes("USA")) {
      usa = ""
    }
    if (input.length >= 3) {
      if (this.regex("[a-zA-Z]+", input) == true) {
        var url = "https://nominatim.openstreetmap.org/?format=json&q=" + input + usa + "&limit=3&email=kalem.byrne@ericsson.com";
        fetch(url).then(function(response) {
          return response.json();
        }).then(function(json) {
          toolboxComponent.searchJson = json;
          try {
            toolboxComponent.opt1 = toolboxComponent.searchJson[0].display_name
            if (toolboxComponent.searchJson.length >= 2) {
              toolboxComponent.opt2 = toolboxComponent.searchJson[1].display_name
            }
            if (toolboxComponent.searchJson.length >= 3) {
              toolboxComponent.opt3 = toolboxComponent.searchJson[2].display_name
            }
            if (toolboxComponent.searchJson[0].display_name == input || toolboxComponent.searchJson[1].display_name == input || toolboxComponent.searchJson[2].display_name == input) {
              toolboxComponent.opt1 = ""
              toolboxComponent.opt2 = ""
              toolboxComponent.opt3 = ""
            }
          }
          catch{
            try {
              if (this.regex("[0-9], USA", input) == true || input.includes("Islands territorial waters") || this.regex("[0-9], United States of America", input) == true) {
                var remove = 0;
                if (input.includes("Islands territorial waters")) { remove = 46 }
                else if (input.includes("United States of America")) { remove = 33 }
                else if (input.includes("USA")) { remove = 12 }
                input = input.substring(0, input.length - remove)
                var url = "https://nominatim.openstreetmap.org/?format=json&q=" + input + "&limit=3&email=kalem.byrne@ericsson.com";
                fetch(url).then(function(response) {
                  return response.json();
                }).then(function(json) {
                  toolboxComponent.searchJson = json

                })
              }
            }
            catch{
            }
          }
        })
      }
    }
  }

  
  coordsFromJSON() {
    var coords = [];
    for (var i = 0; i < 3; i++) {
      if (this.searchJson[i].display_name == this.radiussearchinput1) {
        coords[0] = this.searchJson[i].lon;
        coords[1] = this.searchJson[i].lat;
      }
    }
    this.plotRadius(coords, this.map);
    return coords;
  }



  regex(expression: any, input: string) {
    var regexp = new RegExp(expression, "i");
    var boolean = regexp.test(input);
    return boolean;
  }
}
