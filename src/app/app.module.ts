import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio'
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import {MatTableModule, MatInputModule, MatAutocompleteModule, MatSidenavModule, MatCheckboxModule, MatDialogModule, MatButtonModule, MatIconModule, MatToolbarModule, MatCardModule, MatButtonToggleModule, MatSelectModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import{NgxSpinnerModule} from "ngx-spinner";

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { HttpClientModule } from '@angular/common/http';
import { TableComponent } from './table/table.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { EndnavComponent } from './endnav/endnav.component';
import { AutomoveComponent } from './automove/automove.component';
import { LauncherComponent } from './launcher/launcher.component';
// make sure to install npm install ng2-carouselamos --save

import { Ng2CarouselamosModule } from 'ng2-carouselamos';
//import { SearchsidebarComponent } from './searchsidebar/searchsidebar.component';
import { CdkTableModule } from '@angular/cdk/table';
import { ToastrModule } from 'ngx-toastr';
import { SearchComponent } from './search/search.component'
import {ExcelService} from './excel.service'
import { ToolboxComponent } from './toolbox/toolbox.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TableComponent,
    AutomoveComponent,
    LauncherComponent,
    SidenavComponent,
    EndnavComponent,
    //SearchsidebarComponent,
    SearchComponent,
    ToolboxComponent,

  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatDialogModule,
    Ng2CarouselamosModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatIconModule,
    MatToolbarModule,
    MatCardModule,
    MatAutocompleteModule,
    MatInputModule,
    MatTableModule,
    CdkTableModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    ToastrModule.forRoot({positionClass: 'toast-bottom-left'}),
    NgxSpinnerModule,
    MatSelectModule
  ],
  bootstrap: [AppComponent],
  entryComponents: [TableComponent],
  providers: [ExcelService]
})
export class AppModule { }
