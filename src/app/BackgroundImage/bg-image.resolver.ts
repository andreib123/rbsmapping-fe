import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { map } from 'rxjs/operators';

@Injectable()
export class BackgroundImageResolver implements Resolve<any> {
  constructor(
    private http: HttpClient,
    private sanitizer: DomSanitizer
  ) {}

  resolve = (): Observable<any> =>
  //This uses the http client to get the background image of the map and then
  //configure the responseType into blob(object containing a binary  data)
    this.http.get('assets/images/bg.png', { responseType: 'blob' }).pipe(

      //map it transforms things
      map( image => {

        //so the map will transform the returned data from the blob file into an image.
        const blob: Blob = new Blob([image], { type: 'image/jpeg' });

        //this then creates a url for that image.
        const imageURL = `url(${window.URL.createObjectURL(blob)})`;

        //the DomSanitizer it helps to pass security bugs it basically makes it Safe
        //in DOM context(which is likae a model of a document).
        return this.sanitizer.bypassSecurityTrustStyle(imageURL);
      })
    )
}
