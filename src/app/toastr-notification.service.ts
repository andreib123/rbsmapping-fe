import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastrNotificationService {
  toastOptions: any = {
    positionClass: 'toast-bottom-left'
  }

  constructor(private toastr: ToastrService) { }

  showSuccess(message: string) {
    this.toastr.success(message, "Success")
  }

  showInfo(message: string) {
    this.toastr.info(message, "Info")
  }

  showError(message: string) {
    this.toastr.error(message, "Error")
  }

  toastrGlobalSettings() {}

}
