import { Injectable } from '@angular/core';
import * as ol from "openlayers";
@Injectable({
  providedIn: 'root'
})
export class FeatureSetupService {
  constructor() { }
  setColours(colourBlind: boolean, size: number): any {
    var colour: string = 'rgba(255,244,34,0.65)';
    if (colourBlind !== true) {
      if (size >= 300) {
        colour = 'rgba(254,0,0,0.6)'
        // radius = 20;
      }
      else if (size > 150) {
        colour = 'rgba(254,81,0,0.6)'
        // radius = 15;
      }
      else if (size > 50) {
        colour = 'rgba(254,138,0,0.6)'
        // radius = 12.5;
      }
      else if (size > 20) {
        colour = 'rgba(254,192,0,0.6)'
        // radius = 10;
      }
      else if (size > 5) {
        colour = 'rgba(254,215,0,0.65)'
      }
    }
    else if (colourBlind == true) {
      if (size >= 300) {
        colour = 'rgba(0,145,255,0.6)'
        // radius = 20;
      }
      else if (size > 150) {
        colour = 'rgba(0,229,183,0.6)'
        // radius = 15;
      }
      else if (size > 50) {
        colour = 'rgba(26,255,0,0.6)'
        // radius = 12.5;
      }
      else if (size > 20) {
        colour = 'rgba(0,220,111,0.6)'
        // radius = 10;
      }
      else if (size > 5) {
        colour = 'rgba(240,105,255,0.4)'
      }
    }
    return colour;
  }

  setStyle(feature: ol.Feature | ol.render.Feature, colourBlind: boolean) {
    var styleCache = {};
    var singleStyleCache = {};
    var cacheIndex = 0;
    var size = feature.get('features').length;
    var style = styleCache[size];
    var radius: number;
    var colour = this.setColours(colourBlind, size);
    if (!style) {
      if (size <= 1) {
        style = new ol.style.Style({
          image: new ol.style.Icon({
            //src: 'https://upload.wikimedia.org/wikipedia/commons/2/2a/Dot.jpg'
            src: '../assets/images/blue_tower2.png',
            scale: .06,
          }),
          text: new ol.style.Text({
            text: feature.get('features')[0].get('name').toString(),
            offsetY: -21.5,
            scale: 1.5,
            fill: new ol.style.Fill(
              {
                color: '#CE6A58'
              }
            )
          })
        })
        singleStyleCache[cacheIndex] = style;
        cacheIndex++;
      }
      //feature.get('features').length
      else {
        if (size >= 30) {
          radius = Math.log(size) * 3;
        }
        else {
          radius = 12 + (size / 170);
        } if (size >= 30) {
          radius = Math.log(size) * 3;
        }
        else {
          radius = 12 + (size / 170);
        }

        style = new ol.style.Style({
          image: new ol.style.Circle({
            radius: radius,
            fill: new ol.style.Fill(
              {
                color: colour
              }
            ),
            stroke: new ol.style.Stroke({
              color: colour,
              width: 0
            })
          }),
          text: new ol.style.Text({
            text: size.toString(),
            offsetY: 0,
            scale: 1.25,
            fill: new ol.style.Fill(
              {
                color: '#343634'
              }
            )
          })
        })
        styleCache[size] = style;
      }
    }
    return style;
  }
}
