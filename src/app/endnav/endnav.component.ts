import { Component, OnInit, Input } from '@angular/core';
import { Cell } from '../model/cell.model';

@Component({
  selector: 'app-endnav',
  templateUrl: './endnav.component.html',
  styleUrls: ['./endnav.component.css']
})
export class EndnavComponent implements OnInit {

  constructor() { }
  
  // Inputs cell object from the home component that is passed from the sidenav components mat-select
  @Input() cell: Cell;

  ngOnInit() {
  }
}
