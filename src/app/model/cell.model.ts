export interface Cell {
  geohash: string
  market: string
  region: string
  cellType: string
  cgi: string
  azimuth: string
  freqBand: string
  tac: string
  latitude: string
  enodeBID: string
  longitude: string
  retcapable: string
}
