export interface RbsForInitView {
  numCells: string
  eNodeBID: string
  latitude: string
  longitude: string
  region: string
  tac: string
  retcapable: string
  freqBand: string
  geohashes: string[]
}
