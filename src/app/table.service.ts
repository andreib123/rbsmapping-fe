import { Injectable } from '@angular/core';
import { Cell } from './model/cell.model';

var cells:Cell[];
@Injectable({
  providedIn: 'root'
})

export class TableService {
  constructor() { }

  getCells():Cell[]
  {
    return cells;
  }

  setCells(cellArray:Cell[]){
    cells=[];
    cells=cellArray;
  }
}
